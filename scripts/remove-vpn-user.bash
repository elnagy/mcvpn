#!/bin/bash

set -eu

if [ $# -ne 1 ]; then
    echo "Usage: $0 <email>"
    exit 1
fi


cd /etc/openvpn/state
/opt/easyrsa/easyrsa --batch revoke "$1"
/opt/easyrsa/easyrsa gen-crl
