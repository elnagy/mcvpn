#!/usr/bin/env python 

import smtplib
import os
import sys


username = os.environ['username']
password = os.environ['password']
common_name = os.environ['common_name']

if '@' not in username:
    username = username + '@gmail.com'

if username != common_name:
    print 'username does not match common_name'
    sys.exit(-1)

smtp = smtplib.SMTP_SSL('smtp.gmail.com', 465, 'vpn.nagyinv.com')

if username.endswith('@gmail.com'):
    try:
        smtp.login(username, password)
        print 'smtp auth success'
        sys.exit(0)
    except Exception:
        print 'smtp auth failed'
        sys.exit(-1)
else:
    print 'Unsupported auth domain'
    sys.exit(-1)
